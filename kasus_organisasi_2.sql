#Mencari Nama CEO
SELECT nama FROM employee as CEO
WHERE atasan_id IS NULL;

#Mencari Nama Direktur
SELECT nama AS Direktur FROM employee
WHERE atasan_id = (
    SELECT id FROM employee AS CEO
    WHERE atasan_id IS NULL);

#Mencari Nama Manajer
SELECT nama as Manajer FROM employee
WHERE atasan_id IN (
    SELECT id FROM employee AS Direktur
    WHERE atasan_id = (
         SELECT id FROM employee AS CEO
         WHERE atasan_id IS NULL));

#Mencari Nama Staff 
SELECT nama AS Staff FROM employee
WHERE atasan_id IN (
    SELECT id FROM employee AS Manajer
    WHERE atasan_id IN (
        SELECT id FROM employee AS Direktur
        WHERE atasan_id = (
             SELECT id FROM employee AS CEO
             WHERE atasan_id IS NULL)));

#Mencari Bawahan Pak Budi
SELECT COUNT(*) AS Bawahan_Pak_Budi FROM employee
WHERE atasan_id IN (
    SELECT id FROM employee
    WHERE nama = 'Pak Budi');

#Mencari Bawahan Pak Tono
SELECT COUNT(*) AS Bawahan_Pak_Tono FROM employee
WHERE atasan_id IN (
    SELECT id FROM employee
    WHERE nama = 'Pak Tono');

#Mencari Bawahan Pak Totok
SELECT COUNT(*) AS Bawahan_Pak_Totok FROM employee
WHERE atasan_id IN (
    SELECT id FROM employee
    WHERE nama = 'Pak Totok');

#Mencari Bawahan Bu Sinta
SELECT COUNT(*) AS Bawahan_Bu_Sinta FROM employee
WHERE atasan_id IN (
    SELECT id FROM employee
    WHERE nama = 'Bu Sinta');

#Mencari Bawahan Bu Novi
SELECT COUNT(*) AS Bawahan_Bu_Novi FROM employee
WHERE atasan_id IN (
    SELECT id FROM employee
    WHERE nama = 'Bu Novi');


