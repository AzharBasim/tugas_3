create database sdm;
use sdm;
create table departemen (
    id int (5) auto_increment,
    nama varchar(35),
    primary key (id)
);
insert into departemen values
    ('1', 'Manajemen'),
    ('2', 'Pengembangan Bisnis'),
    ('3','Teknisi'),
    ('4', 'analisis');

create table karyawan
(
    id int(5) not null auto_increment,
    nama varchar(35),
    jenis_kelamin enum('L', 'P'),
    status enum('menikah', 'belum'),
    tanggal_lahir date,
    tanggal_masuk date,
    Departemen int (5),
    primary key (id),
    constraint foreign key (Departemen) references departemen(id)
);

insert into karyawan values
('1', 'Rizki Saputra', 'L', 'menikah', '1980-10-11',
 '2011-1-1', '1'),
('2', 'Farhan reza', 'L', 'belum', '1989-11-1',
 '2011-1-1', '1'),
('3', 'Riyando Adi', 'L', 'menikah', '1977-1-25',
 '2011-1-1', '1'),
('4', 'Diego Manuel', 'L', 'menikah', '1983-2-22',
 '2012-9-4', '2'),
('5', 'Satya Laksana', 'L', 'menikah', '1981-1-12',
 '2011-3-19', '2'),
('6', 'Miguel Hernandez', 'L', 'menikah', '1994-10-16',
 '2014-6-15', '2'),
('7', 'Putri Persada', 'P', 'menikah', '1988-1-30',
 '2013-4-14', '2'),
('8', 'Alma Safira', 'P', 'menikah', '1991-5-18',
 '2013-9-28', '3'),
('9', 'Haqi Hafiz', 'L', 'belum', '1995-9-19',
 '2015-3-9', '3'),
('10', 'Abi Isyawara', 'L', 'belum', '1991-6-3',
 '2012-1-22', '3'),
('11', 'Maman Kresna', 'L', 'belum', '1993-8-21',
 '2012-9-15', '3'),
('12', 'Nadia Aulia', 'P', 'belum', '1989-10-7',
 '2012-5-7', '4'),
('13', 'Mutiara Rezki', 'P', 'menikah', '1988-3-23',
 '2013-5-21', '4'),
('14', 'Dani Setiawan', 'L', 'belum', '1986-2-11',
 '2014-11-30', '4'),
('15', 'Budi Putra', 'L', 'belum', '1995-10-23',
 '2015-12-3', '4');


