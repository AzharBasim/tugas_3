create table organisasi (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  jabatan varchar(30) NOT NULL,
  nama varchar(50),
  atasan_id int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (atasan_id) REFERENCES organisasi (id) );

INSERT INTO organisasi (jabatan, nama, atasan_id)
VALUES ('Direktur', 'Rizki Saputra', NULL);

INSERT INTO organisasi (Jabatan, nama, atasan_id)
VALUES ('Manajer', 'Farhan Reza', '1'),
       ('Manajer', 'Riyando Adi', '1');

INSERT INTO organisasi (Jabatan, nama, atasan_id)
VALUES ('Staff Pengembangan Bisnis', 'Acep Gorbacep', '2'),
       ('Staff Teknisi', 'Agus Kusnandar', '2'),
       ('Staff Analis', 'Ujang Ajun', '3');

SELECT
    nama
FROM
    organisasi
WHERE
    atasan_id > 1;